#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

const int MATRIXSIZE = 10;
const int MAXVALUE = 14, MINVALUE = -14;

void printMatrix(int(*matrix)[MATRIXSIZE]);
void sortMatrix(bool (*func)(int a, int b), int(*matrix)[MATRIXSIZE]);
void fillMatrix(int(*matrix)[MATRIXSIZE]);
void fillZeroRowsWithUsersRow(int(*matrix)[MATRIXSIZE], int usersRow[MATRIXSIZE]);
bool comparsion_1(int a, int b);
bool comparsion_2(int a, int b);
int countSumUnderDiagonal(int(*matrix)[MATRIXSIZE]);

int main()
{
    srand(time(nullptr));

    int matrix[MATRIXSIZE][MATRIXSIZE]{};
    int usersRow[MATRIXSIZE]{};

    fillMatrix(matrix);
    printMatrix(matrix);
    sortMatrix(comparsion_1, matrix);
    sortMatrix(comparsion_2, matrix);
    printMatrix(matrix);
    fillZeroRowsWithUsersRow(matrix, usersRow);
    cout << countSumUnderDiagonal(matrix);
    return 0;
}

void printMatrix(int(*matrix)[MATRIXSIZE])
{
    cout << "\n\n";
    for (int i{}; i < MATRIXSIZE; i++) {
        for (int j{}; j < MATRIXSIZE; j++) {
            cout << setw(4) << matrix[i][j];
        }
        cout << "\n";
    }
}

void sortMatrix(bool (*func)(int a, int b), int(*matrix)[MATRIXSIZE])
{
    for (int row{}; row < MATRIXSIZE; row++) {
        for (int i{}; i < MATRIXSIZE; i++) {
            for (int j = MATRIXSIZE - 1; j > i; j--) {
                if (func(matrix[row][j - 1], matrix[row][j]))
                {
                    int t = matrix[row][j];
                    matrix[row][j] = matrix[row][j - 1];
                    matrix[row][j - 1] = t;
                }
            }
        }
    }
}

void fillMatrix(int(*matrix)[MATRIXSIZE])
{
    for (int i{}; i < MATRIXSIZE; i++) {
        for (int j{}; j < MATRIXSIZE; j++) {
            matrix[i][j] = MINVALUE + rand() % (MAXVALUE - MINVALUE + 1);
        }
    }
}

void fillZeroRowsWithUsersRow(int(*matrix)[MATRIXSIZE], int usersRow[MATRIXSIZE])
{
    cout << "\nEnter your vector to replace all-zero rows\n";
    for (int i{}; i < MATRIXSIZE; i++) {
        cout << "Item of number " << i + 1 << ": ";
        cin >> usersRow[i];
    }
    for (int row{}; row < MATRIXSIZE; row++) {
        bool flagZero = true;
        for (int i{}; i < MATRIXSIZE; i++) {
            if (matrix[row][i] != 0)
            {
                flagZero = false;
            }
        }
        if (flagZero)
        {
            for (int i{}; i < MATRIXSIZE; i++) {
                matrix[row][i] = usersRow[i];
            }
        }
    }
}

bool comparsion_1(int a, int b)
{
    return a > b;
}

bool comparsion_2(int a, int b)
{
    return a == 0;
}

int countSumUnderDiagonal(int(*matrix)[MATRIXSIZE])
{
    int sum = 0;
    for (int i{}; i < MATRIXSIZE; i++) {
        for (int j{}; j <= i; j++) {
            if (matrix[i][j] < 0)
            {
                sum += matrix[i][j];
            }
        }
    }
    return sum;
}